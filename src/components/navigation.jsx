import logo from "../imgs/logo black.png";
export const Navigation = (props) => {
  return (
    <nav id='menu' className='navbar navbar-default navbar-fixed-top'>
      <div className='container'>
        <div className='navbar-header'>
          <button
            type='button'
            className='navbar-toggle collapsed'
            data-toggle='collapse'
            data-target='#bs-example-navbar-collapse-1'
          >
            {' '}
            <span className='sr-only'>Toggle navigation</span>{' '}
            <span className='icon-bar'></span>{' '}
            <span className='icon-bar'></span>{' '}
            <span className='icon-bar'></span>{' '}
          </button>
          <a className='navbar-brand2 page-scroll' href='#page-top'>
            <img src={logo} alt="logo" className="logo"/>
          </a>{' '}
        </div>

        <div
          className='collapse navbar-collapse'
          id='bs-example-navbar-collapse-1'
        >
          <ul className='nav navbar-nav navbar-right'>
            <li>
              <a href='#features' className='page-scroll'>
                <span className="orange">F</span>eatures
              </a>
            </li>
            <li>
              <a href='#about' className='page-scroll'>
                <span className="pink">A</span>bout
              </a>
            </li>
            <li>
              <a href='#services' className='page-scroll'>
                <span className="orange">S</span>ervices
              </a>
            </li>
            <li>
              <a href='#portfolio' className='page-scroll'>
                <span className="pink">G</span>allery
              </a>
            </li>
            <li>
              <a href='#testimonials' className='page-scroll'>
                <span className="orange">T</span>estimonials
              </a>
            </li>
            <li>
              <a href='#team' className='page-scroll'>
                <span className="pink">T</span>eam
              </a>
            </li>
            <li>
              <a href='#contact' className='page-scroll'>
                <span className="orange">C</span>ontact
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}
