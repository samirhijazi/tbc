import { Image } from "./image";
import logo from "../imgs/temp.jpeg"

export const Gallery = (props) => {
  return (
    <div id='portfolio' className='text-center'>
      <div className='container'>
        <div className='section-title'>
          <h2>Gallery</h2>
          <p>
           
          </p>
        </div>
        <div className='row'>
          <div className='portfolio-items'>
            
                <div className='col-sm-6 col-md-4 col-lg-4'>
                  <Image  title="Business Strategy" largeImage={logo} smallImage={logo}/>
                </div>
                <div className='col-sm-6 col-md-4 col-lg-4'>
                  <Image  title="People and change" largeImage={logo} smallImage={logo}/>
                </div>
                <div className='col-sm-6 col-md-4 col-lg-4'>
                  <Image  title="Operational and Process Excellence" largeImage={logo} smallImage={logo}/>
                </div>
                <div className='col-sm-6 col-md-4 col-lg-4'>
                  <Image  title="Entreprise Protfolio and Program" largeImage={logo} smallImage={logo}/>
                </div>
                <div className='col-sm-6 col-md-4 col-lg-4'>
                  <Image  title="People and Change" largeImage={logo} smallImage={logo}/>
                </div>

          </div>
        </div>
      </div>
    </div>
  )
  }
