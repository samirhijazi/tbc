export const Test = () =>{
return (
    <div id="test" aria-hidden="true">
      <div className="container">
        <div className="row">
          <div className="col-xs-12 col-md-6">
            {" "}
            <img src="img/about.jpg" className="img-responsive" alt="" />{" "}
          </div>
          <div className="col-xs-12 col-md-6">
            <div className="about-text">
              <h2>About Us</h2>
              <p>Whether you're experiencing challenges with company culture, internal processes or need a new business strategy to reflect your envisioned future, we're here to help. We stand by your side to adapt to today's market dynamics and continue to compete no matter the threats you might be facing. We can scale our services to support change efforts ranging from tactical improvements through large scale transformation efforts.</p>
              <h3>We Are All in This Toghether</h3>
              <div className="list-style">
                <div className="col-lg-6 col-sm-6 col-xs-12">
                Many companies are struggling to navigate today's challenges for several reasons, such as:
                  <ul>
                    <li>Lack of understanding of business and customer insights.</li>
                    <li>Uncertainty about current or future market realities.</li>
                    <li>Misaligned vision and strategy.</li>
                    <li>Difficulty innovating or quickly launching new capabilities.</li>
                    <li>Poor strategy execution and value realization.</li>
                    <li>Culture of complacency or change fatigue.</li>
                    <li>Overall lack of business agility across employees, process and technology.</li>
                    <li className="gold">Not sure what you need?
Contact us to learn hay to keep pace in an Ever-changing business and economy world.</li>
                  </ul>
                </div>
                
              </div>
            </div>
          </div>
          <div className="toghether">
          Successfully navigating <span className="gold">the Complex Business Challenges</span> means being prepared to dive deep into your business and market to gain clarity on issues and opportunities.
          Then, you can establish a compelling and inspiring vision, develop world-class capabilities, build a culture of agility, and deliver value in a rapid, agile fashion. <br /><br />
          But you don't have to do it alone - we in <span className="gold">The Business Complement</span> will work with you as an extension of your team to ensure you can improve productivity,
          scale your organization, stay ahead of the competition, and more.
          We can guide you to a Business Anywhere future one where every interaction is smart and seamless, every process is fully automated and paperless, and everyone is ready for whatever the future holds.
          </div>
        </div>
      </div>
    </div>
);
}