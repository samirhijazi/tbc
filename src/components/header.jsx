export const Header = (props) => {
  return (
    <header id='header'>
      <div className='intro'>
        <div className='overlay'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-8 col-md-offset-2 intro-text'>
                <h1>
                <a className='navbar-brand page-scroll' href='#page-top'>
            <span className="first-letter">T</span>he <span className="first-letter">B</span>usiness <span className="first-letter">C</span>omplement
          </a>{' '}
                </h1>
                <p></p>
                <a
                  href='#features'
                  className='btn btn-custom btn-lg page-scroll'
                >
                  Learn More
                </a>{' '}
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  )
}
